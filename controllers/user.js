const { response, request } = 'express';

const usuarioGet = (req = request, res = response) => {

  //const query = req.query;
  const { nombre, key = 'no key' } = req.query;

  res.json({
    msg: 'get API - controlador',
    nombre,
    key
  });
};

const usuarioPost = (req, res = response) => {

  //req.body trae la informacion del body
  const {nombre, edad} = req.body;

  res.status(201).json({
    msg: 'post API',
    nombre,
    edad
  });
}

const usuarioPut = (req, res = response) => {

  //const id = req.params.id;
  const { id } = req.params;

  res.json({
    msg: 'put API',
    id
  });
};

module.exports = {
  usuarioGet,
  usuarioPost,
  usuarioPut
};