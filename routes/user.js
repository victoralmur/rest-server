const { Router } = require('express');
const 
{ usuarioGet,
  usuarioPost, 
  usuarioPut
} = require('../controllers/user');

const router = Router();

router.get('/', usuarioGet);

router.put('/:id', usuarioPut);

router.post('/', usuarioPost);

router.delete('/', (req, res) => {
  res.json({
    msg: 'delete API'
  });
});

module.exports = router;